//
//  RHApiManager.swift
//  Rancho Humilde
//
//  Created by Sanchan on 16/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class RHApiManager

{
    static let sharedManager:RHApiManager = RHApiManager()
    func postDataWithJson(url:String,parameters:[String:[String:AnyObject]],completion:@escaping (_ responseDict:Any?,_ error:Error?,_ isDone:Bool)->Void)
    {
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { response in
            do
            {
                let post:Any = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                completion(post,nil,true)
            }
            catch
            {
                completion(nil,response.result.error!,false)
            }
        }
    }
    
}
